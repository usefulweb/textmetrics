const getLeftIndex = ( pixels, width ) => {
  const len = pixels.length,
    height = len / ( width * 4 );

  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
      const alphaIndex = ( j * width * 4 ) + ( i * 4 ) + 3;
      if (pixels[ alphaIndex ] > 0) {
        return i;
      }
    }
  }
  return 0;
};

const getRightIndex = ( pixels, width ) => {
  const len = pixels.length,
    height = len / ( width * 4 );
  for (let i = width; --i;) {
    for (let j = 0; j < height; j++) {
      const alphaIndex = ( j * width * 4 ) + ( i * 4 ) + 3;
      if (pixels[ alphaIndex ] > 0) {
        return i;
      }
    }
  }
  return width;
};

const getTopIndex = ( pixels, width ) => {
  for (let i = 3, n = pixels.length; i < n; i += 4) {
    if (pixels[ i ] > 0) {
      return ( i - 3 ) / ( 4 * width );
    }
  }
  return pixels.length / ( 4 * width );
};

const getBottomIndex = ( pixels, width ) => {
  for (let i = pixels.length - 1; i >= 3; i -= 4) {
    if (pixels[ i ] > 0) {
      return ( i - 3 ) / ( 4 * width );
    }
  }
  return 0;
};

export default {
  getBottomIndex,
  getLeftIndex,
  getRightIndex,
  getTopIndex
};
