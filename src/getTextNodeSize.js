const getTextNodeSize = params => {
  const {
      fontSize,
      fontFamily,
      text
    } = params,
    font = `${fontSize}px ${fontFamily}`,
    node = document.createElement( 'div' );

  node.textContent = text;
  node.style = {
    position: 'fixed',
    left: '100vw',
    top: '100vh',
    whiteSpace: 'nowrap',
    display: 'inline-block',
    font
  };
  document.body.appendChild( node );
  const width = node.offsetWidth,
    height = node.offsetHeight;

  node.remove();

  return { width, height };
};

export default getTextNodeSize;
