import getTextNodeSize from './getTextNodeSize';
import { getTopIndex, getBottomIndex, getLeftIndex, getRightIndex } from './imageData';

const getTextSize = params => {
  const {
      fontFamily,
      fontWeight = 'normal',
      precision = 1,
      text
    } = params,
    fontSize = params.fontSize * precision,
    padding = fontSize * 0.5,
    canvas = document.createElement( 'canvas' ),
    ctx = canvas.getContext( '2d' ),
    { nodeWidth, nodeHeight } = getTextNodeSize({
      ...params,
      fontSize
    }),
    canvasWidth = nodeWidth,
    canvasHeight = fontSize * 2 + padding;

  canvas.width = canvasWidth;
  canvas.height = canvasHeight;
  ctx.font = `${fontWeight} ${fontSize}px ${fontFamily}`;
  ctx.textBaseline = 'top';
  ctx.textAlign = 'center';

  ctx.clearRect( 0, 0, canvasWidth, canvasHeight );
  ctx.fillText( text, canvas.width / 2, padding, canvasWidth );

  const pixels = ctx.getImageData( 0, 0, canvasWidth, canvasHeight ).data;

  const top = getTopIndex( pixels, canvasWidth ) / precision,
    bottom = getBottomIndex( pixels, canvasWidth ) / precision,
    left = getLeftIndex( pixels, canvasWidth ) / precision,
    right = getRightIndex( pixels, canvasWidth ) / precision,
    width = right - left,
    height = ( bottom - top ),
    heightScale = height / params.fontSize,
    scaledWidth = width / heightScale;

  return {
    scaledWidth,
    heightScale,
    nodeWidth: nodeWidth / precision,
    nodeHeight: nodeHeight / precision,
    height,
    width
  };
};

export default {
  getTextSize
};
