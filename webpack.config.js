const path = require( 'path' );

module.exports = {
  entry: [
    path.join( __dirname, '/src/index.js' )
  ],
  resolve: {
    alias: {},
    extensions: [ '.js' ]
  },
  stats: {
    colors: true
  },
  output: {
    path: path.join( __dirname, '/dist' ),
    filename: 'index.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules)/,
        use: [
          'babel-loader'
        ]
      }
    ]
  }
};
